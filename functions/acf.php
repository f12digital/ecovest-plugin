<?php
// change save and load path for acf-json
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point($path){
	// update path
	$path = plugin_dir_path(__FILE__) . '/acf-json';
	// return
	return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point($paths){
	// remove original path (optional)
	unset($paths[0]);
	// append path
	$paths[] = plugin_dir_path(__FILE__) . '/acf-json';
	// return
	return $paths;
}



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Ecovest Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Homepage Slider',
		'menu_title'	=> 'Homepage Slider',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Team Slider',
		'menu_title'	=> 'Team Slider',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Portfolio Slider',
		'menu_title'	=> 'Portfolio Slider',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Header Settings',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
}