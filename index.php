<?php 
/**
 * Plugin Name: Ecovest
 */

wp_register_script ( 'slick-script', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), true);
wp_register_script ( 'custom-script',  plugin_dir_url( __FILE__ ) . 'frontend/dist/js/min/main-min.js', array('jquery'), true);
wp_register_script ( 'fa-script', 'https://use.fontawesome.com/8093bcf28e.js' );

wp_enqueue_script ( ['custom-script', 'slick-script', 'fa-script'] );

// load FE css
function myplugin_scripts() {
    wp_register_style( 'slick-styles',  '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
    wp_register_style( 'ecovest-styles',  plugin_dir_url( __FILE__ ) . 'frontend/dist/css/style.css' );
    wp_enqueue_style( ['ecovest-styles', 'slick-styles'] );
}
add_action( 'wp_enqueue_scripts', 'myplugin_scripts' );


// includes
require_once('functions/acf.php');
require_once('functions/cpt.php');
require_once('shortcodes/homepage-slider.php');
require_once('shortcodes/team-slider.php');
require_once('shortcodes/portfolio-slider.php');


add_filter( 'widget_text', 'do_shortcode' );