<?php

add_shortcode('team-slider', 'register_team_slider');
function register_team_slider( $atts = [], $content = null) {

$content = '<div class="team-slider-component columns col-2">';
$content .= '<div class="col team-slider-component--slider">';
$content .= '<div class="team-slider">';
            if( have_rows('team_slider', 'options') ):
            while ( have_rows('team_slider', 'options') ) : the_row();
            $content .='<div class="slide">';
                $content .= '<div class="team-members-wrap">';
                $members = get_sub_field('members');
                if( $members ):
                    foreach( $members as $post ):
                        setup_postdata($post);
                        $content .= '<div class="team-member" data-content="'.get_the_content($post->ID).'">';
                            $content .= '<img src="'.get_field('headshot',$post->ID)['url'].'" />';
                            $content .= '<div class="team-member-content">';
                                $content .= '<h5 class="heading--neutral-light heading--h5 heading--fw-200">'.get_the_title($post->ID).'</h5>';
                                $content .= '<h6 class="heading--neutral-light heading--h6 heading--fw-200">'.get_field('title',$post->ID).'</h6>';
                            $content .= '</div>';
                        $content .= '</div>';
                    endforeach;
                    // Reset the global post object so that the rest of the page works correctly.
                    wp_reset_postdata();
                endif;
                $content .= '</div>';
            $content .='</div>';
            endwhile;
            else :
            // no rows found
            endif;
$content .= '</div>';
$content .= '</div>';
$content .= '<div class="col team-slider-component--content">';
    $i = 0;
    $title = get_field('section_title', 'options');
    $button = get_field('section_button', 'options');
    if( have_rows('team_slider','options') ):
        while ( have_rows('team_slider','options') ) : the_row();
            $bodyCopy = get_sub_field('blurb');
            $content .='<div class="slide-content '.(($i === 0) ? 'slide-content-active' : ''). ' slide-content-'.$i.'">';
            $content .='<h2 class="heading heading--section heading--section--neutral-light">'.$title.'</h2>';
            $content .='<div class="slider-controls">';
            $content .='<a href="#prev" class="prev"><i class="fa fa-chevron-left"></i></a>';
            $content .='<a href="#next" class="next"><i class="fa fa-chevron-right"></i></a>';
            $content .='</div>';
            $content .='<div class="slide-content--content">'.$bodyCopy.'</div>';
                if($button) {
                    $buttonTitle = $button['title'];
                    $buttonUrl = $button['url'];
                    $buttonTarget = $button['target'];
                    $content .= '<a target="'.$buttonTarget.'" href="'.$buttonUrl.'" class="button button--primary-light--inverted">'.$buttonTitle.'<i class="fa fa-arrow-right"></i></a>';
                }
                $content .= '</div>';
        $i++;
    endwhile;
    endif;
    $content .='<div class="team-slider-component--popup">';
            $content .='<a hre="#close" class="team-slider-component--popup-close"><i class="fa fa-times"></i></a>';
                $content .= '<div class="team-member">';
                    $content .= '<img src="" />';
                    $content .= '<div class="team-member-content">';
                        $content .= '<h5 class="heading--neutral-light heading--h5 heading--fw-200"></h5>';
                        $content .= '<h6 class="heading--neutral-light heading--h6 heading--fw-200"></h6>';
                    $content .= '</div>';
                $content .= '</div>';
                $content .= '<div class="team-member--content"></div>';
            $content .='</a>';
    $content .='</div>';
$content .= '</div>';
    // always return
    return $content;
}