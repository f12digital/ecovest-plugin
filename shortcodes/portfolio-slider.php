<?php

add_shortcode('portfolio-slider', 'register_portfolio_slider');
function register_portfolio_slider( $atts = [], $content = null) {
    $content .= '<div class="portfolio-slider-component columns col-2">';
        $content .='<div class="col portfolio-slider-component--slider">';
            $content .='<div class="portfolio-slider">';
                        if( have_rows('portfolio_slider','options') ):
                        while ( have_rows('portfolio_slider','options') ) : the_row();
                        $image = get_sub_field('image');
                        $content .='<div>';
                        $content .='<div class="slide" style="background-image: url('.$image['url'].')"></div>';
                        $content .='</div>';
                        endwhile;
                        else :
                        // no rows found
                        endif;
            $content .='</div>';
        $content .='</div>';
        $content .='<div class="col portfolio-slider-component--content">';
                    $i = 0;
                    if( have_rows('portfolio_slider','options') ):
                    while ( have_rows('portfolio_slider','options') ) : the_row();
                        $title = get_sub_field('title');
                        $bodyCopy = get_sub_field('content');
                        $button = get_sub_field('button');
                        $content .='<div class="slide-content '.(($i === 0) ? 'slide-content-active' : ''). ' slide-content-'.$i.'">';
                        $content .='<h2 class="heading heading--section heading--section--primary-medium">'.$title.'</h2>';
                        $content .='<div class="slider-controls">';
                        $content .='<a href="#prev" class="prev"><i class="fa fa-chevron-left"></i></a>';
                        $content .='<a href="#next" class="next"><i class="fa fa-chevron-right"></i></a>';
                        $content .='</div>';
                        $content .='<div class="slide-content--content">'.$bodyCopy.'</div>';
                            if($button) {
                                $buttonTitle = $button['title'];
                                $buttonUrl = $button['url'];
                                $buttonTarget = $button['target'];
                                $content .= '<a target="'.$buttonTarget.'" href="'.$buttonUrl.'" class="button button--primary-light--inverted">'.$buttonTitle.'<i class="fa fa-arrow-right"></i></a>';
                            }
                    $content .='</div>';
                        $i++;
                    endwhile;
                    else :
                    // no rows found
                    endif;
        $content .='</div>';
        if( have_rows('portfolio_slider','options') ):
            $n = 0;
            while ( have_rows('portfolio_slider','options') ) : the_row();
                $content .= '<div class="'. (($n==0) ? 'portfolio-blocks-active' : '').' portfolio-blocks portfolio-blocks-'.$n.'">';
                if( have_rows('content_blocks','options') ):
                    while ( have_rows('content_blocks','options') ) : the_row();
                        $theme = get_sub_field('theme');
                        $title = get_sub_field('title');
                        $blockContent = get_sub_field('content');
                        $blockSecondaryContent = get_sub_field('secondary_content');
                        $img = get_sub_field('image');
                        $imgUrl = $img['url'];
                        $imgAlt = $img['alt'];
                        $content .= '<div class="portfolio-blocks--block '.$theme.'">';
                            $content .= '<div class="portfolio-blocks--block--content">';
                                $content .= '<h3>'.$title.'</h3>';
                                $content .= '<div class="portfolio-blocks--block--content-wrap">';
                                    $content .= '<div class="portfolio-blocks--block--content-wrap-content">'.$blockContent.'</div>';
                                    $content .= '<div class="portfolio-blocks--block--content-wrap-content-secondary"><div>'.$blockSecondaryContent.'</div></div>';
                                $content .= '</div>'; // end block wrap content
                            $content .= '</div>'; // end block content
                            $content .= '<div class="portfolio-blocks--block--img">';
                                $content .= '<img src="'.$imgUrl.'" alt="'.$imgAlt.'" />';
                            $content .= '</div>'; // end block img
                            $content .= '<div class="background-decoration"></div>';
                        $content .= '</div>'; // end block
                    endwhile;
                endif;
                $content .= '</div>'; // end blocks
                $n++;
            endwhile;
        endif;
    $content .='</div>';
// always return
return $content;
}