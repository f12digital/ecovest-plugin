<?php

add_shortcode('ecovest-slider', 'register_homepage_slider');
function register_homepage_slider( $atts = [], $content = null) {
    $content .= '<div class="homepage-slider-component columns col-2">';
        $content .='<div class="col homepage-slider-component--slider">';
            $content .='<div class="homepage-slider">';
                        if( have_rows('slider','options') ):
                        while ( have_rows('slider','options') ) : the_row();
                        $image = get_sub_field('image');
                        $content .='<div>';
                        $content .='<div class="slide" style="background-image: url('.$image['url'].')"></div>';
                        $content .='</div>';
                        endwhile;
                        else :
                        // no rows found
                        endif;
            $content .='</div>';
        $content .='</div>';
        $content .='<div class="col homepage-slider-component--content">';
                    $i = 0;
                    if( have_rows('slider','options') ):
                    while ( have_rows('slider','options') ) : the_row();
                        $title = get_sub_field('title');
                        $bodyCopy = get_sub_field('content');
                        $button = get_sub_field('button');
                        $content .='<div class="slide-content '.(($i === 0) ? 'slide-content-active' : ''). ' slide-content-'.$i.'">';
                        $content .='<h2 class="heading heading--section heading--section--secondary">'.$title.'</h2>';
                        $content .='<div class="slider-controls">';
                        $content .='<a href="#prev" class="prev"><i class="fa fa-chevron-left"></i></a>';
                        $content .='<a href="#next" class="next"><i class="fa fa-chevron-right"></i></a>';
                        $content .='</div>';
                        $content .='<div class="slide-content--content">'.$bodyCopy.'</div>';
                            if($button) {
                                $buttonTitle = $button['title'];
                                $buttonUrl = $button['url'];
                                $buttonTarget = $button['target'];
                                $content .= '<a target="'.$buttonTarget.'" href="'.$buttonUrl.'" class="button button--primary-light--inverted">'.$buttonTitle.'<i class="fa fa-arrow-right"></i></a>';
                            }
                    $content .='</div>';
                        $i++;
                    endwhile;
                    else :
                    // no rows found
                    endif;
        $content .='</div>';
    $content .='</div>';
// always return
return $content;
}