jQuery(function ($) {
	$(document).ready(function () {
		$('.homepage-slider').slick({
			centerMode: true,
			slidesToShow: 1,
			arrows: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
			// appendArrows: $('.slider-controls'),
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 3,
					},
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 1,
					},
				},
			],
		});
		$('.homepage-slider').on('afterChange', function(event, slick, direction){
            var activeSlide = $('.slick-active').attr('data-slick-index');
            $('.slide-content-active').removeClass('slide-content-active');
            $('.slide-content-'+activeSlide).addClass('slide-content-active');
        });

		$('.team-slider').slick({
			slidesToShow: 1,
			arrows: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
		});
		$('.team-slider').on('afterChange', function(event, slick, direction){
            var activeSlide = $('.slick-active').attr('data-slick-index');
            $('.slide-content-active').removeClass('slide-content-active');
            $('.slide-content-'+activeSlide).addClass('slide-content-active');
        });




		$('.team-member').on('click',function(){
			var name = $(this).find('h5').text();
			var title = $(this).find('h6').text();
			var img = $(this).find('img').attr('src');
			var content = $(this).attr('data-content');
			$('.team-slider-component--popup img').attr('src', img);
			$('.team-slider-component--popup h5').html(name);
			$('.team-slider-component--popup h6').html(title);
			$('.team-slider-component--popup .team-member--content').html(content);
			$('.team-slider-component--popup').css({'display': 'flex'});
			$('.team-slider-component').addClass('expanded');
		});

		$('.team-slider-component--popup-close').on('click', function(e){
			e.preventDefault();
			$('.team-slider-component--popup').css({'display': 'none'});
			$('.team-slider-component').removeClass('expanded');
		})




		$('.portfolio-slider').slick({
			centerMode: true,
			slidesToShow: 1,
			arrows: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
			// appendArrows: $('.slider-controls'),
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 3,
					},
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '40px',
						slidesToShow: 1,
					},
				},
			],
		});
		$('.portfolio-slider').on('afterChange', function(event, slick, direction){
            var activeSlide = $('.slick-active').attr('data-slick-index');
            $('.slide-content-active').removeClass('slide-content-active');
            $('.slide-content-'+activeSlide).addClass('slide-content-active');

			$('.portfolio-blocks-active').removeClass('portfolio-blocks-active');
			$('.portfolio-blocks-'+activeSlide).addClass('portfolio-blocks-active');
        });



	}); //end ready
});
