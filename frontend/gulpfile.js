var gulp = require('gulp');
var sass = require('gulp-sass')(require('node-sass'));
var runSequence = require('run-sequence');
var cleanCSS = require('gulp-clean-css');
var cssbeautify = require('gulp-cssbeautify');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const minifyJS = require('gulp-minify');

// Development Tasks
// -----------------

gulp.task('sass', function () {
	return gulp
		.src('src/scss/*.scss') // Gets all files ending with .scss in app/scss and children dirs
		.pipe(sourcemaps.init()) // init source maps
		.pipe(sass({ outFile: 'assets/css/style.css' }).on('error', sass.logError)) // Passes it through a gulp-sass, log errors to console
		.pipe(sourcemaps.write()) // write sourcemaps
		.pipe(gulp.dest('dist/css/')); // Outputs it in the css folder
});

gulp.task('watch', function () {
	gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
	gulp.watch('src/*.html');
	gulp.watch('src/js/*.js', gulp.series('babel'));
});

// beautify css
gulp.task('beautify-css', function () {
	return gulp.src('dist/css/*.css').pipe(cssbeautify()).pipe(gulp.dest('dist/css/'));
});

gulp.task('babel', () =>
	gulp
		.src('src/js/*.js')
		.pipe(sourcemaps.init())
		.pipe(
			babel({
				presets: ['@babel/env'],
			})
		)
		.pipe(concat('main.js'))
		.pipe(minifyJS())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('dist/js/min'))
);

// Optimization Tasks
// ------------------
gulp.task('minify-css', () => {
	return gulp
		.src('dist/css/*.css')
		.pipe(cleanCSS({ compatibility: 'ie8' }))
		.pipe(gulp.dest('dist/css/'));
});


gulp.task('minify-js', () => {
	return gulp
		.src('dist/js/*.js')
		.pipe(minifyJS())
		.pipe(gulp.dest('dist/js/min/'));
});

gulp.task('default', gulp.series('sass', 'beautify-css', 'minify-css', 'babel', 'watch', function (done) {
    done();
}));

gulp.task('build', gulp.series('sass', 'minify-css', 'babel', function (done) {
    done();
}));